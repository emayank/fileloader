#include <string>
#include <fstream>
#include <vector>

using namespace std;

class Sorter
{
private:
	ifstream _infile;
	ofstream _outfile;

public:
	Sorter(const string& infile, const string& outfile);
	~Sorter();

	void sort();
	void sort(vector<string>& lines);
	void save(const vector<string>& lines);
};
