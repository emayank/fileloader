#include "indexer/Indexer.h"
#include <fstream>

using namespace std;

Indexer::Indexer()
{
}

Indexer::Indexer(const string& infile)
{
	_infile.open(infile.c_str());
}

Indexer::~Indexer()
{
	_infile.close();
}

void Indexer::createIndex()
{
	char ch = 0;
	int count = 0;
	int offset = 0;
	string line;
	while (!_infile.eof())
	{
		getline(_infile, line);
		if (ch != line[0])
		{
			if (ch == 0) //If this is the first line of rules file
			{
				count = 1;
			}
			else
			{
				addIndex(ch, offset, count);
				offset = _infile.tellg();
				count = 0;
			}
			ch = line[0];
		}
		else
		{
			count++;
		}
	}
}

void Indexer::addIndex(char ch, int offset, int count)
{
	string key;
	key += ch;
	pair<int, int> index(offset, count);
	_index.insert(pair<string, pair<int, int>>(key, index));
	//_index[key] = index;
}

string Indexer::findRule(const string& command_base)
{
	string key;
	key += command_base[0];
	pair<int, int> index = getIndex(key);
	int offset = index.first;
	int count = index.second;
	if (offset == -1)
	{
		return "";
	}

	_infile.seekg(offset, ios_base::beg); //seek or move the file pointer to the offset with respect to beg (begin of the file)
	string rule;
	while(count--)
	{
		getline(_infile, rule);
		if (rule.find(command_base))
		{
			return rule;
		}
	}
	return "";
}

const pair<int, int>& Indexer::getIndex(const string& key)
{
	map<string, pair<int, int>>::iterator iter = _index.find(key);
	if (iter != _index.end()) //found
	{
		return iter->second;
	}
	return pair<int, int>(-1, -1);
}
