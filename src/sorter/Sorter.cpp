#include "sorter/Sorter.h"
#include <algorithm>

Sorter::Sorter(const string& infile, const string& outfile)
{
	_infile.open(infile.c_str());
	_outfile.open(outfile.c_str());
}

void Sorter::sort()
{
	vector<string> lines;
	sort(lines);
	save(lines);
}

void Sorter::sort(vector<string>& lines)
{
	string line;
	while(_infile.eof())
	{
		getline(_infile, line);
		line.erase(remove_if(line.begin(), line.end(), ::isspace), line.end());
		if (!line.empty())
		{
			lines.push_back(line);
		}
	}
	_infile.close();
	std::sort(lines.begin(), lines.end()); //std namespace to resolve that its under std namespace and not inside Sorter class
}

void Sorter::save(const vector<string>& lines)
{
	for(vector<string>::const_iterator iter = lines.begin(); iter != lines.end(); ++iter) //const_iterator is taken as the 'lines' is const reference
	{
		_outfile << *iter << endl;
	}
	_outfile.close();
}

Sorter::~Sorter()
{
	if (_infile.is_open())
	{
		_infile.close();
	}
	if (_outfile.is_open())
	{
		_outfile.close();
	}
}
