#include <map>
#include <fstream>

using namespace std;

class Indexer
{
private:
	ifstream _infile;
	map<string, pair<int, int>> _index;

public:
	Indexer();
	Indexer(const string& infile);
	~Indexer();

	void createIndex();
	void addIndex(char ch, int offset, int count);
	string findRule(const string& command_base);
	const pair<int, int>& getIndex(const string& key);
};
