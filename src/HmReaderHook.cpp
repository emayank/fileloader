#include "HmReader.h"
#include <string>
#include "sorter/Sorter.h"
#include "indexer/Indexer.h"

using namespace std;

class HmReaderHook: public HmReader //HmReader class exists in HmReader.h. HmReader is a class in Hypermesh code that I inherited and overridden
{/////////////////////////////////////One function was load and other was executeCommand. Now when Hypermesh is loaded it will call the function written in the
private: /////////////////////////////Derived class (HMReaderHook). When a file is loaded fetchRule of HmReaderHook will be called...
	Indexer _indexer;

public:
	void load(const string& infile)
	{
		HmReader::load(infile);
		string outfile = infile + ".index";
		Sorter sorter(infile, outfile);
		sorter.sort();
		_indexer = Indexer(outfile);
		_indexer.createIndex();
	}

	string fetchRule(const string& command)
	{
		int parenthesis_index = command.find('(');
		string command_base = command.substr(0, parenthesis_index);
		return _indexer.findRule(command_base);
	}
};
